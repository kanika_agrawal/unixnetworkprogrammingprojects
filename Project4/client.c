#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <netinet/in.h>

#define MAXBUF 2000

#define FILE_READ 0
#define FILE_DELETE 1
#define SERVER_RESPONSE 2
#define EXIT 3

#define USE_UDP 1
#define USE_TCP 2

pid_t server_pid = -1;

typedef struct
{
    int mesg_len;
    int mesg_type;
    char mesg_data[MAXBUF];
} Mesg;


Mesg getUserInputRequest()
{
    char command[500];
    char opcode[10];
    char filename[400];
    Mesg msg;
    
    while(1)
    {    
        int i=0;
        
        //read user input
	bzero(command, 100);
        bzero(opcode, 10);
        bzero(filename, 50);
        
        printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");
     
        if(fgets(command, sizeof(command), stdin) == NULL)
        {
            printf("No command entered..please try again.\n");
            continue;
        }

        if(strlen(command) > 0)
        {
            command[strlen(command) - 1] = '\0';
        
        }
        else
        {
            printf("Please try again..\n");
            continue;
        }

        //Extract the opcode
        while(command[i] != ' ' && command[i] != '\0') 
        {	
            opcode[i] = command[i];
            i++;
        }

        opcode[i] = '\0';	

        //Check is user wants to exit
        if(strcasecmp(opcode, "EXIT")== 0 )
        {
            msg.mesg_type = EXIT;
            return msg;
        }

        //Check if the opcode is correct or not
        else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
        {
            int j=0;
            //Remove white spaces
            while(command[i] == ' ')
                    i++;

            //Copy filename form the command
            while(command[i]!='\0')
            {
                    filename[j] = command[i];
                    i++;
                    j++;
            }
            filename[j] = '\0';

            //Check if the filename is given or not
            if(strlen(filename)==0)
            {
                    printf("Filename not entered\n");
                    continue;
            }

            //update the message		
            msg.mesg_len = strlen(filename);
            strcpy(msg.mesg_data ,filename);

            if(strcasecmp(opcode, "READ") == 0)
            {
                    msg.mesg_type = FILE_READ;
            }
            else
            {
                    msg.mesg_type = FILE_DELETE;
            }

            return msg;
        }
        
        //incorrect opcode
        else
        {
            printf("Incorrect opcodes..please enter again\n");
            continue;
        }
    }	
}

int main(int argc, char *argv[])
{
	int ipc_mode = USE_UDP;
	char *ipc = "UDP";
	char *ip_addr;
	int port_number;
		
	if (argc != 4 )
    	{
        	printf("Input format: ./client <Protocol> <Server IP ADDRESS> <Server Port Number>\n");
		exit(1);	
    	}

	ipc = argv[1];
	ip_addr = argv[2];
	port_number = atoi(argv[3]);

	printf("IP Address of the server: %s\n", ip_addr);
	printf("Port Number : %d\n", port_number);
	
	if(strcasecmp(ipc, "UDP")== 0)
		ipc_mode = USE_UDP;
	
	else if(strcasecmp(ipc, "TCP")== 0)
		ipc_mode = USE_TCP;

	else
		ipc_mode = 0;
 

	switch(ipc_mode) 
	{
	    case USE_UDP:
	    {
       		int clientSocket, portNum, nBytes;
        	char buffer[MAXBUF];
        	struct sockaddr_in serverAddr;
        	socklen_t addr_size;

		/*Create UDP socket*/
        	/*AF_INET =  Address Family of IP version 4*/
        	/*SOCK_STREAM = connectionless UDP protocol*/
        	/*0  = IP Protocol*/
        	clientSocket = socket(AF_INET, SOCK_DGRAM, 0);
		if(clientSocket < 0)
		{
			printf("Error in creating the socket..Client Exiting..\n");
                        exit(1);
		}
		
        	/*Configure settings in address struct*/

        	serverAddr.sin_family = AF_INET;

        	/*Convert the unsigned short integer hostshort from host byte order to network byte order*/
        	serverAddr.sin_port = htons(port_number);

        	/*Convert IP address into long format*/
        	serverAddr.sin_addr.s_addr = inet_addr(ip_addr);//inet_addr("129.210.16.75");// inet_addr("127.0.0.1");

        	/*length of useful data in sockaddr_in is shorter than sockaddr hence,*/
        	/*Set all bits of the padding field to 0 */
        	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

        	/*Initialize size variable to be used later on*/
        	addr_size = sizeof serverAddr;

		while(1)
                {
                        Mesg msgToSend = getUserInputRequest();
			char message[MAXBUF];
                        bzero(message,MAXBUF);
                        bzero(buffer, MAXBUF);

                        if(msgToSend.mesg_type == EXIT)        
			{
				strcpy(message, "Exit");
                        	if(sendto(clientSocket,message,strlen(message) + 1 ,0,(struct sockaddr *)&serverAddr,addr_size) < 0)
                        	{
                                	printf("Error in sending the request to the server.\n");
                        	}
				break;
                        }
			
			/*char message[MAXBUF];
                	bzero(message,MAXBUF);
                	bzero(buffer, MAXBUF);*/

                	if(msgToSend.mesg_type == FILE_READ)
                	{
                        	strcpy(message, "Read ");
                        	strcat(message, msgToSend.mesg_data);
                	}
                	else
                	{
                        	strcpy(message, "Delete ");
                        	strcat(message, msgToSend.mesg_data);
                	}

                	/*Send message to server*/
                	if(sendto(clientSocket,message,strlen(message) + 1 ,0,(struct sockaddr *)&serverAddr,addr_size) < 0)
			{
				printf("Error in sending the request to the server.\n");
				break;
			}

                	/*Receive message from server*/
                	/*ssize_t recvfrom(int socket, void *restrict buffer, size_t length,int flags, struct sockaddr *restrict address,socklen_t *restrict address_len);*/
                	nBytes = recvfrom(clientSocket,buffer,MAXBUF,0, NULL, NULL);
			if(nBytes < 0)
			{
				printf("Error in receiving the server response.\n");
				break;
			}

               	 	printf("Received from server: %s\n",buffer); 
                }

        	printf("Client Exiting..\n");
		break;
	    }

            case USE_TCP:
            {       
		int sock;
    		struct sockaddr_in server;
    		char message[MAXBUF] , server_reply[MAXBUF];

      		/*Create socket*/
    		/*AF_INET =  Address Family of IP version 4*/
   		/*SOCK_STREAM = connection oriented TCP/IP protocol*/
    		/*0  = IP Protocol*/

    		sock = socket(AF_INET , SOCK_STREAM , 0);
    		if (sock == -1)
    		{
        		printf("Could not create socket...Client Exiting...\n");
        		exit(1);
    		}
    		printf("Socket created...\n");

    		/*Convert IP address into long format*/
    		server.sin_addr.s_addr = inet_addr(ip_addr);
    		server.sin_family = AF_INET;

    		/*Convert the unsigned short integer hostshort from host byte order to network byte order*/
    		server.sin_port = htons(port_number);

    		/*Connect to remote server*/
    		if(connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    		{
        		printf("Connect failed..Client Exiting...\n");
        		exit(1);
    		}

    		printf("Client Connected to remote server...\n");

    		/*keep communicating with server*/		
		while(1)
                {
                        Mesg msgToSend = getUserInputRequest();

        		if(msgToSend.mesg_type == EXIT)
        		{
                		break;
        		}

        		char message[MAXBUF];
        		bzero(message,MAXBUF);
        		bzero(server_reply, MAXBUF);

        		if(msgToSend.mesg_type == FILE_READ)
        		{
                		strcpy(message, "Read ");
                		strcat(message, msgToSend.mesg_data);
        		}
        		else
        		{
                		strcpy(message, "Delete ");
                		strcat(message, msgToSend.mesg_data);
        		}

        		/*Send request*/
        		if(send(sock , message , strlen(message) , 0 /*flags*/) < 0)
        		{
            			printf("Send failed.\n");
            			break;
        		}

        		/*Receive a reply from the server*/
        		if(recv(sock , server_reply ,MAXBUF , 0 /*flags*/) < 0)
        		{
            			printf("Receive failed...Client Exiting..\n");
            			break;
        		}

        		printf("Server replies : %s", server_reply);
                }

		/*Perform Cleanups*/

		/*Close the socket*/
		if(close(sock) < 0)
    		{
        		printf("Client Socket not closed properly.\n");
    		}

		printf("Client Exiting..\n");
               	break;
	    }
                
            default:
            {
                printf("invalid IPC selected. Terminating\n");
                break;
            }
	}
	
        return 0;
}

