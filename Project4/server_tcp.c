#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>

#define MAXBUF 2000

char* ReadFile(char filename[1024])
{
	off_t file_size = 0;
	FILE *file_to_read_fp = NULL;
	char *msg  = malloc(MAXBUF * sizeof(char));
	bzero(msg, MAXBUF);
	
	/*Check if the given path is a file or directory*/
	struct stat s;
	if(stat(filename, &s) == 0)
	{
		if(!(s.st_mode & S_IFREG))
		{
			sprintf(msg,"Cannot read the file [%s] as path refers to the directory.\n", filename);
			return msg;
		}
	}
	
	/*Check if the file to be read from exists*/
 	int rval = access (filename, F_OK);
 	if (rval != 0) 
	{
 		if (errno == ENOENT) 
   			sprintf(msg,"Cannot read the file [%s] as it does not exist.\n", filename);
  		else if (errno == EACCES) 
   			sprintf(msg,"Cannot read the file [%s] as it is not accessible.\n", filename);
  		return msg;
 	}

 	/*Check read access of the file to be read from */
 	rval = access (filename, R_OK);
 	if (rval != 0)
  	{
  		sprintf(msg,"Access denied to the file [%s].\n", filename);		
		return msg;
	}

	/*Check the file size*/
	if(stat(filename, &s) == 0)
        file_size =  s.st_size;
        if(file_size > MAXBUF)
        {
                sprintf(msg,"Cannot read the file [%s] as its size is bigger than the allocated buffer.\n", filename);
                return msg;
        }


	/*Open the file to be read*/
	file_to_read_fp = fopen(filename, "r");
        if (file_to_read_fp == NULL)
        {
                sprintf(msg,"Cannot open the [%s] file to read from.\n", filename);
		return msg;
        }
        
	fread(msg, 1, MAXBUF, file_to_read_fp);
        fclose(file_to_read_fp);

	msg[strlen(msg)] = '\0';
	return msg;
}

char* DeleteFile(char filename[1024])
{	
	/*delete the file*/
	char *msg  = malloc(MAXBUF * sizeof(char));
        bzero(msg, MAXBUF);

	int status = unlink(filename);
        if( status == 0 )
        {
	        sprintf(msg,"file [%s] deleted successfully.\n",filename);
	}
	else
	{
		if(errno  == EISDIR)
		{
			sprintf(msg,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
		}
		else if(errno  == EACCES)
		{
			sprintf(msg,"Cannot delete the file [%s] as it is not accessible.\n", filename);
		}
		else if(errno == ENOENT)
		{
			sprintf(msg,"Cannot find the file [%s] for deletion. \n", filename);
		}
        	else
        	{
                	sprintf(msg,"Unable to delete the file [%s].\n", filename);
        	}
	}

        return msg;
}

char* PerformFileOperations(char *data)
{
        char opcode[10];
        char filename[1024];
	int i = 0;
	int j = 0;
	
	char *msg;
	/*cleaning up char arrays*/
	bzero(opcode, 10);
	bzero(filename, 1024);

	/*read opcode*/
        while(data[i] != ' ' && data[i] != '\0')
        {
            opcode[i] = data[i];
            i++;
        }
        opcode[i] = '\0';

	/*remove white spaces from the middle*/
	while(data[i] == ' ')
        {
		i++;
	}

	/*read filename*/
	while(data[i]!='\0')
        {
              filename[j] = data[i];
              i++;
              j++;
       	}
        filename[j] = '\0';

	/*Call appropriate functions according to the opcode*/
        if(strcasecmp(opcode, "READ")==0)
        {
                msg  = ReadFile(filename);
        }
        else if(strcasecmp(opcode,"DELETE")== 0)
        {
                 msg = DeleteFile(filename);
        }
	else
	{
		sprintf(msg, "Unknown opcode..%s\n", opcode);
	}
	return msg;
}


 
int main()
{
    int socket_desc , client_sock , c , read_size;
    struct sockaddr_in server , client;
    char client_message[MAXBUF];
    char *msgToSend = NULL;
     
    /*Create socket*/
    socket_desc = socket(AF_INET , SOCK_STREAM , 0);
    if (socket_desc == -1)
    {
        printf("Could not create socket. Server Exiting");
	exit(1);
    }
    printf("Socket created.\n");

    /*Get the IP address and the port number of the server*/
    struct ifreq ifr;
    char array[] = "eth0";

    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name , array , IFNAMSIZ - 1);
    ioctl(socket_desc, SIOCGIFADDR, &ifr);
 
    char *ip_addr =  inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr);
    int port_number;   

    /*Prepare the sockaddr_in structure*/
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(ip_addr);
    server.sin_port = 0;
     
    /*Bind*/
    if(bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
    {
        /*print the error message*/
	perror("Bind Failed");
        printf("Server Exiting..\n");
        exit(1);
    }

    /*Print the Port Number*/
    struct sockaddr_in sin;
    socklen_t len = sizeof(sin);
    if (getsockname(socket_desc, (struct sockaddr *)&sin, &len) == -1)
    {               
                perror("getsockname");
                exit(1);
    }
    else
    {       
                port_number = ntohs(sin.sin_port);
    }

    printf("Bind Done!\n");
    printf("Server IP Address is %s - %s\n" , array , ip_addr );
    printf("Server Port Number is: %d\n", port_number);
	 
    /*Listen for incoming connections*/
    if(listen(socket_desc , 3) < 0)
    {
	printf("Error in listening to the socket..Server Exiting.\n");
	exit(1);
    }
     
    /*Accept incoming connection*/
    printf("Waiting for incoming connections...\n");
    c = sizeof(struct sockaddr_in);
 
    /*accept connection from an incoming client*/
    client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
    if (client_sock < 0)
    {
        perror("accept failed.");
        exit(1);
    }
     
    /*Receive a message from client*/
    while( (read_size = recv(client_sock , client_message , MAXBUF , 0 /*flags*/)) > 0 )
    {
	client_message[read_size] = '\0';

	/*Process the Client Request*/
	msgToSend = PerformFileOperations(client_message);
	        
	//Send the reply to the client
	if(write(client_sock , msgToSend , strlen(msgToSend)) < 0)
	{
		printf("Error in sending response to the client..Server Exiting.\n");
		break;
	}

	bzero(client_message, MAXBUF);
	free(msgToSend);
    }
     
    if(read_size == 0)
    {
        printf("Client disconnected...Server Exiting...\n");
        fflush(stdout);
    }
    else if(read_size == -1)
    {
        perror("recv failed");
    }
     
    return 0;
}
