#include <stdio.h>
#include <stdlib.h> 
#include <string.h>   
#include <sys/socket.h>   
#include <arpa/inet.h> 
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
 
#define SEM_CLIENT "kanika_12345"

#define MAXBUF 2000
#define FILE_READ 0
#define FILE_DELETE 1
#define SERVER_RESPONSE 2
#define EXIT 3

pid_t server_pid = -1;

typedef struct
{
    int mesg_len;
    int mesg_type;
    char mesg_data[MAXBUF];
} Mesg;


Mesg getUserInputRequest()
{
    char command[500];
    char opcode[10];
    char filename[400];
    Mesg msg;
    
    while(1)
    {    
        int i=0;
        
        /*read user input*/
	bzero(command, 100);
        bzero(opcode, 10);
        bzero(filename, 50);
        
        printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");
     
        if(fgets(command, sizeof(command), stdin) == NULL)
        {
            printf("No command entered..please try again.\n");
            continue;
        }

        if(strlen(command) > 0)
        {
            command[strlen(command) - 1] = '\0';
        
        }
        else
        {
            printf("Please try again..\n");
            continue;
        }

        /*Extract the opcode*/
        while(command[i] != ' ' && command[i] != '\0') 
        {	
            opcode[i] = command[i];
            i++;
        }

        opcode[i] = '\0';	

        /*Check is user wants to exit*/
        if(strcasecmp(opcode, "EXIT")== 0 )
        {
            msg.mesg_type = EXIT;
            return msg;
        }

        /*Check if the opcode is correct or not*/
        else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
        {
            int j=0;
            /*Remove white spaces*/
            while(command[i] == ' ')
                    i++;

            /*Copy filename form the command*/
            while(command[i]!='\0')
            {
                    filename[j] = command[i];
                    i++;
                    j++;
            }
            filename[j] = '\0';

            /*Check if the filename is given or not*/
            if(strlen(filename)==0)
            {
                    printf("Filename not entered\n");
                    continue;
            }

            /*update the message*/		
            msg.mesg_len = strlen(filename);
            strcpy(msg.mesg_data ,filename);

            if(strcasecmp(opcode, "READ") == 0)
            {
                    msg.mesg_type = FILE_READ;
            }
            else
            {
                    msg.mesg_type = FILE_DELETE;
            }

            return msg;
        }
        
        /*incorrect opcode*/
        else
        {
            printf("Incorrect opcodes..please enter again\n");
            continue;
        }
    }	
}


int main(int argc , char *argv[])
{
    int sock;
    struct sockaddr_in server;
    char message[MAXBUF] , server_reply[MAXBUF];
     
    /*Create the Posix Semaphore*/
    sem_t *client_sem;

    /*Client Semaphore*/
    client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
    if(client_sem == SEM_FAILED)
    {
                if(errno == EEXIST)
                {
                        printf("Client Semaphore already exists..deleting it.\n");
                        if(sem_unlink(SEM_CLIENT)< 0 )
                        {
                                        printf("Cannot delete the rogue Client Semaphore...program exiting.\n");
                                        exit(1);
                        }

                        client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
                        if(client_sem == SEM_FAILED)
                        {
                                printf("Cannot create Client Semaphore...program exiting.\n");
                                exit(1);
                        }
                }
                else
                {
                        printf("Error in creating the Client Sempahore..Deleting Shared Memory..Program Exiting..\n");
                        exit(1);
                }

     }

     /*Fork the Server*/
     if((server_pid = fork())< 0)
     {
     	printf("Fork Failed!\n");
        exit(1);
     }
     if(server_pid == 0)
     {
     	execvp("./server_tcp", NULL);
     }
        
     if(sem_wait(client_sem) < 0)
     {
     	printf("Error in taking the lock on the client side...Client Exiting.\n");
        exit(1);
     }


    /*Create socket*/
    /*AF_INET =  Address Family of IP version 4*/
    /*SOCK_STREAM = connection oriented TCP/IP protocol*/
    /* 0  = IP Protocol*/

    sock = socket(AF_INET , SOCK_STREAM , 0);
    if (sock == -1)
    {
        printf("Could not create socket...Client Exiting...\n");
	exit(1);
    }
    printf("Socket created...\n");
    
    /*Convert IP address into long format*/
    server.sin_addr.s_addr = inet_addr("127.0.0.1");
    server.sin_family = AF_INET;

    /*Convert the unsigned short integer hostshort from host byte order to network byte order*/
    server.sin_port = htons(8888);
 
    /*Connect to remote server*/
    if (connect(sock , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        printf("Connect failed..Client Exiting...\n");
        exit(1);
    }
     
    printf("Client Connected to remote server...\n");
     
    /*keep communicating with server*/
    while(1)
    {
        
	Mesg msgToSend = getUserInputRequest();

        if(msgToSend.mesg_type == EXIT)
        {
        	break;
        }

        char message[MAXBUF];
        bzero(message,MAXBUF);
	bzero(server_reply, MAXBUF);

        if(msgToSend.mesg_type == FILE_READ)
        {
        	strcpy(message, "Read ");
                strcat(message, msgToSend.mesg_data);
        }
        else
        {
        	strcpy(message, "Delete ");
                strcat(message, msgToSend.mesg_data);
        }
	
        /*Send some data*/
        if(send(sock , message , strlen(message) , 0 /*flags*/) < 0)
        {
            printf("Send failed.\n");
            break;
        }
         
        /*Receive a reply from the server*/
        if(recv(sock , server_reply ,MAXBUF , 0 /*flags*/) < 0)
        {
            printf("Receive failed...Client Exiting..\n");
            break;
        }
         
        printf("Server replies : %s", server_reply);
    }
     
    if(close(sock) < 0)
    {
	printf("Client Socket not closed properly.\n");
    }
	
    if(sem_unlink(SEM_CLIENT) < 0)
    {
    	printf("Cannot delete the Client Semaphore.\n");
    }

    printf("Client Exiting..\n");
    return 0;
}
