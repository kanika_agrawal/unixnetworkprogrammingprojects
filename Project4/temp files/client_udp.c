#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>

#define FILE_READ 0
#define FILE_DELETE 1
#define SERVER_RESPONSE 2
#define EXIT 3

#define SEM_CLIENT "kanika_12345"
pid_t server_pid = -1;

#define MAXBUF 2000

typedef struct
{
    int mesg_len;
    int mesg_type;
    char mesg_data[MAXBUF];
} Mesg;


Mesg getUserInputRequest()
{
    char command[500];
    char opcode[10];
    char filename[400];
    Mesg msg;

    while(1)
    {
        int i=0;

        /*read user input*/
        bzero(command, 100);
        bzero(opcode, 10);
        bzero(filename, 50);

        printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");

        if(fgets(command, sizeof(command), stdin) == NULL)
        {
            printf("No command entered..please try again.\n");
            continue;
        }

        if(strlen(command) > 0)
        {
            command[strlen(command) - 1] = '\0';

        }
        else
        {
            printf("Please try again..\n");
            continue;
        }

        /*Extract the opcode*/
        while(command[i] != ' ' && command[i] != '\0')
        {
            opcode[i] = command[i];
            i++;
        }

        opcode[i] = '\0';

        /*Check is user wants to exit*/
        if(strcasecmp(opcode, "EXIT")== 0 )
        {
            msg.mesg_type = EXIT;
            return msg;
        }
	
	/*Check if the opcode is correct or not*/
        else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
        {
            int j=0;
            /*Remove white spaces*/
            while(command[i] == ' ')
                    i++;

            /*Copy filename form the command*/
            while(command[i]!='\0')
            {
                    filename[j] = command[i];
                    i++;
                    j++;
            }
            filename[j] = '\0';

            /*Check if the filename is given or not*/
            if(strlen(filename)==0)
            {
                    printf("Filename not entered\n");
                    continue;
            }

            /*update the message*/
            msg.mesg_len = strlen(filename);
            strcpy(msg.mesg_data ,filename);

            if(strcasecmp(opcode, "READ") == 0)
            {
                    msg.mesg_type = FILE_READ;
            }
            else
            {
                    msg.mesg_type = FILE_DELETE;
            }

            return msg;
        }

        /*incorrect opcode*/
        else
        {
            printf("Incorrect opcodes..please enter again\n");
            continue;
        }
    }
}



int main()
{
	int clientSocket, portNum, nBytes;
  	char buffer[MAXBUF];
  	struct sockaddr_in serverAddr;
  	socklen_t addr_size;


	/*Create the Posix Semaphore*/
        sem_t *client_sem;

        /*Client Semaphore*/
        client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
        if(client_sem == SEM_FAILED)
        {
                if(errno == EEXIST)
                {
                        printf("Client Semaphore already exists..deleting it.\n");
                        if(sem_unlink(SEM_CLIENT)< 0 )
			{
					printf("Cannot delete the rogue Client Semaphore...program exiting.\n");
					exit(1);	
			}

                        client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
                        if(client_sem == SEM_FAILED)
                        {
                                printf("Cannot create Client Semaphore...program exiting.\n");
                                exit(1);
                        }
                }
		else
		{
			printf("Error in creating the Client Sempahore..Deleting Shared Memory..Program Exiting..\n");
                        exit(1);
		}
		 
          }
	
	/*Fork the Server*/
	if((server_pid = fork())< 0)
        {
        	printf("Fork Failed!\n");
                exit(1);
        }
        if(server_pid == 0)
        {
        	execvp("./server_udp", NULL);
        }
	
	if(sem_wait(client_sem) < 0)
	{
		printf("Error in taking the lock on the client side...Client Exiting.\n");
		exit(1);
	}

  	/*Create UDP socket*/
    	/*AF_INET =  Address Family of IP version 4*/
    	/*SOCK_STREAM = connectionless UDP protocol*/
    	/*0  = IP Protocol*/
  	clientSocket = socket(AF_INET, SOCK_DGRAM, 0);

  	/*Configure settings in address struct*/
  	
	serverAddr.sin_family = AF_INET;
	
	/*Convert the unsigned short integer hostshort from host byte order to network byte order*/
  	serverAddr.sin_port = htons(7891);
	
	/*Convert IP address into long format*/
  	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
  	
	/*length of useful data in sockaddr_in is shorter than sockaddr hence,*/
	/* Set all bits of the padding field to 0 */
	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

  	/*Initialize size variable to be used later on*/
  	addr_size = sizeof serverAddr;

  	while(1)
	{
    		Mesg msgToSend = getUserInputRequest();

        	if(msgToSend.mesg_type == EXIT)
        	{
                	break;
        	}

        	char message[MAXBUF];
        	bzero(message,MAXBUF);
        	bzero(buffer, MAXBUF);

        	if(msgToSend.mesg_type == FILE_READ)
        	{
                	strcpy(message, "Read ");
                	strcat(message, msgToSend.mesg_data);
        	}
        	else
        	{
                	strcpy(message, "Delete ");
                	strcat(message, msgToSend.mesg_data);
        	}

    		/*Send message to server*/
    		sendto(clientSocket,message,strlen(message) + 1 ,0,(struct sockaddr *)&serverAddr,addr_size);

    		/*Receive message from server*/
		/*ssize_t recvfrom(int socket, void *restrict buffer, size_t length,int flags, struct sockaddr *restrict address,socklen_t *restrict address_len);*/
                nBytes = recvfrom(clientSocket,buffer,MAXBUF,0, NULL, NULL);

    		printf("Received from server: %s\n",buffer);    		
  	}

	if(kill(server_pid, SIGINT))
        {
         	printf("Interrupt Signal cannot be sent to the Server.");
        }

	if(sem_unlink(SEM_CLIENT) < 0)
        {
        	printf("Cannot delete the Client Semaphore.\n");
        }

	printf("Client Exiting..\n");
  	return 0;
}
