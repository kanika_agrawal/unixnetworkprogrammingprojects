#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

#define MAXBUF 2000

char* ReadFile(char filename[1024])
{
        off_t file_size = 0;
        FILE *file_to_read_fp = NULL;
        char *msg  = malloc(MAXBUF * sizeof(char));
        bzero(msg, MAXBUF);

        /*Check if the given path is a file or directory*/
        struct stat s;
        if(stat(filename, &s) == 0)
        {
                if(!(s.st_mode & S_IFREG))
                {
                        sprintf(msg,"Cannot read the file [%s] as path refers to the directory.\n", filename);
                        return msg;
                }
        }

        /*Check if the file to be read from exists*/
        int rval = access (filename, F_OK);
        if (rval != 0)
        {
                if (errno == ENOENT)
                        sprintf(msg,"Cannot read the file [%s] as it does not exist\n", filename);
                else if (errno == EACCES)
                        sprintf(msg,"Cannot read the file [%s] as it is not accessible\n", filename);
                return msg;
        }

        /*Check read access of the file to be read from */
        rval = access (filename, R_OK);
        if (rval != 0)
        {
                sprintf(msg,"Access denied to the file [%s].\n", filename);
                return msg;
        }

        /*Check the file size*/
        if(stat(filename, &s) == 0)
        file_size =  s.st_size;
        if(file_size > MAXBUF)
        {
                sprintf(msg,"Cannot read the file [%s] as its size is bigger than the allocated buffer.\n",filename);
                return msg;
        }


        /*Open the file to be read*/
        file_to_read_fp = fopen(filename, "r");
        if (file_to_read_fp == NULL)
        {
                sprintf(msg,"Cannot open the file [%s] to read from.\n", filename);
                return msg;
        }

        fread(msg, 1, MAXBUF, file_to_read_fp);
        fclose(file_to_read_fp);
        return msg;
}

char* DeleteFile(char filename[1024])
{
        /*delete the file*/
        char *msg  = malloc(MAXBUF * sizeof(char));
        bzero(msg, MAXBUF);

        int status = unlink(filename);
        if( status == 0 )
        {
                sprintf(msg,"file [%s] deleted successfully.\n",filename);
        }
        else
        {
                if(errno  == EISDIR)
                {
                        sprintf(msg,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
                }
                else if(errno  == EACCES)
                {
                        sprintf(msg,"Cannot delete the file [%s] as it is not accessible.\n", filename);
                }
                else if(errno == ENOENT)
                {
                        sprintf(msg,"Cannot find the file [%s] for deletion. \n", filename);
                }
                else
                {
                        sprintf(msg,"Unable to delete the file [%s].\n", filename);
                }
        }

        return msg;
}


char* PerformFileOperations(char *data)
{
        char opcode[10];
        char filename[1024];
        int i = 0;
        int j = 0;
        char *msg;

        /*cleaning up char arrays*/
        bzero(opcode, 10);
        bzero(filename, 1024);

        /*ead opcode*/
        while(data[i] != ' ' && data[i] != '\0')
        {
            opcode[i] = data[i];
            i++;
        }
        opcode[i] = '\0';

        /*remove white spaces from the middle*/
        while(data[i] == ' ')
        {
                i++;
        }

        /*read filename*/
        while(data[i]!='\0')
        {
              filename[j] = data[i];
              i++;
              j++;
        }
        filename[j] = '\0';

        /*Call appropriate functions according to the opcode*/
        if(strcasecmp(opcode, "READ")==0)
        {
                msg  = ReadFile(filename);
        }
        else if(strcasecmp(opcode,"DELETE")== 0)
        {
                 msg = DeleteFile(filename);
        }
        else
        {
                sprintf(msg, "Unknown opcode..%s\n", opcode);
        }
        return msg;
}

int main()
{
  	int udpSocket, nBytes;
  	char client_message[MAXBUF];
	char *msgToSend = NULL;
  	struct sockaddr_in serverAddr, clientAddr;
  	struct sockaddr_storage serverStorage;
 	socklen_t addr_size, client_addr_size;
  	int i;

  	/*Create UDP socket*/
  	udpSocket = socket(AF_INET, SOCK_DGRAM, 0);

	/*Get the Ip Address and the port number*/
	struct ifreq ifr;
    	char array[] = "eth0";

    	ifr.ifr_addr.sa_family = AF_INET;
    	strncpy(ifr.ifr_name , array , IFNAMSIZ - 1);
    	ioctl(udpSocket, SIOCGIFADDR, &ifr);

	char *ip_addr = inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr);	
    	int port_number;
	
  	/*Configure settings in address struct*/
  	serverAddr.sin_family = AF_INET;
  	serverAddr.sin_port = 0;
  	serverAddr.sin_addr.s_addr = inet_addr(ip_addr);
  	memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);  

  	/*Bind socket with address struct*/
  	if(bind(udpSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr)) < 0)
	{
		printf("Error in binding the socket..Server Exiting.\n");
		exit(1);
	}

	/*Print the Port Number*/
	struct sockaddr_in sin;
	socklen_t len = sizeof(sin);
	if (getsockname(udpSocket, (struct sockaddr *)&sin, &len) == -1)
        {
		perror("getsockname");
		exit(1);
	}
	else
	{	
		port_number = ntohs(sin.sin_port);
	}

	printf("Bind Done!\n");
	printf("Server IP Address is %s - %s\n" , array , ip_addr );
        printf("Server Port Number is: %d.\n", port_number);
  	/*Initialize size variable to be used later on*/
  	addr_size = sizeof serverStorage;

  	while(1)
	{
    		/* Address and port of requesting client will be stored on serverStorage variable */
    		nBytes = recvfrom(udpSocket,client_message,MAXBUF,0,(struct sockaddr *)&serverStorage, &addr_size);
		if(nBytes < 0)
		{
			printf("Error in receiving the request from client..Server Exiting.\n");
			break;
		}
	
		if(strcasecmp(client_message, "Exit") == 0)
		{
			printf("Client Disconnected..Server Exiting.\n");
			break;
		}	
	
        	/*Process the Client Request*/
        	msgToSend = PerformFileOperations(client_message);
    		
    		/*Send reply to the client*/
    		if(sendto(udpSocket,msgToSend,MAXBUF,0,(struct sockaddr *)&serverStorage,addr_size) < 0)
		{
			printf("Error in sending response to the client..Server Exiting.\n");
			break;
		}
		
     		free(msgToSend);
  	}

  	return 0;
}
