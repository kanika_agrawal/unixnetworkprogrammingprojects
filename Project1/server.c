#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>

volatile sig_atomic_t server_flag = 0;
volatile sig_atomic_t exit_server_flag = 0;
struct sigaction newAction, oldAction;

char *shared_filename = "/tmp/KA_shared_file.txt";
pid_t client_pid;

void ReadFile(char filename[1024])
{
        FILE *shared_file_fp, *file_to_read_fp;
        char ch;

	shared_file_fp = fopen(shared_filename, "w");

	if(shared_file_fp == NULL)
	{	
		printf("Not able to open the shared file...Server Exiting\n");
		exit_server_flag=1;
		return;
	}
	
	//Check if the given path is a file or directory
	struct stat s;
	if(stat(filename, &s) == 0)
	{
		if(!(s.st_mode & S_IFREG))
		{
			fprintf(shared_file_fp,"Cannot read the file [%s] as path refers to the directory.\n", filename);
			fclose(shared_file_fp);
			return;
		}
	}
	
	//Check if the file to be read from exists
 	int rval = access (filename, F_OK);
 	if (rval != 0) 
	{
 		if (errno == ENOENT) 
   			fprintf (shared_file_fp,"Cannot read the file [%s] as it does not exist\n", filename);
  		else if (errno == EACCES) 
   			fprintf (shared_file_fp,"Cannot read the file [%s] as it is not accessible\n", filename);
		
		fclose(shared_file_fp);
  		return;
 	}

 	//Check read accessof the file to be read from 
 	rval = access (filename, R_OK);
 	if (rval != 0)
  	{
  		fprintf (shared_file_fp,"Access denied to the file [%s].\n", filename);		
		fclose(shared_file_fp);
		return;
	}

	//Open the file to be read
	file_to_read_fp = fopen(filename, "r");
        if (file_to_read_fp == NULL)
        {
                fprintf(shared_file_fp,"Cannot open the file to read from.\n");
                fclose(shared_file_fp);
		return;
        }

        while (1)
        {
                ch = fgetc(file_to_read_fp);

                if (ch == EOF)
                        break;
                else
                {
                        putc(ch, shared_file_fp);
                }
        }

        fclose(file_to_read_fp);
        fclose(shared_file_fp);
}

void DeleteFile(char filename[1024])
{
        FILE *shared_file_fp;
        shared_file_fp = fopen(shared_filename, "w");
        if (shared_file_fp == NULL)
        {
                printf("Not able to open the shared file...Server Exiting\n");
               	exit_server_flag=1;
		return;
        }
	
	//delete the file       
	int status = unlink(filename);
        if( status == 0 )
        {
	        fprintf(shared_file_fp,"file [%s] deleted successfully.\n",filename);
	}
	else
	{
		if(errno  == EISDIR)
		{
			fprintf(shared_file_fp,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
		}
		else if(errno  == EACCES)
		{
			fprintf(shared_file_fp,"Cannot delete the file [%s] as it is not accessible.\n", filename);
		}
		else if(errno == ENOENT)
		{
			fprintf(shared_file_fp,"Cannot find the file [%s] for deletion. \n", filename);
		}
        	else
        	{
                	fprintf(shared_file_fp,"Unable to delete the file [%s].\n", filename);
        	}
	}

        fclose(shared_file_fp);
}

void PerformFileOperations()
{
        char opcode[10];
        char filename[1024];

        FILE *shared_file_fp;
        shared_file_fp = fopen (shared_filename, "r") ;
        if (shared_file_fp == NULL)
        {
                printf("Cannot open shared file..Server Exiting");
                exit_server_flag=1;
                return;
        }
	
	// cleaning up char arrays
	bzero(opcode, 10);
	bzero(filename, 1024);

	// read opcode and filename
        fscanf(shared_file_fp,"%s %s",opcode, filename);

	//close the shared file
        fclose(shared_file_fp);

        if(strcasecmp(opcode, "READ")==0)
        {
                ReadFile(filename);
        }
        else if(strcasecmp(opcode,"DELETE")== 0)
        {
                 DeleteFile(filename);
        }
	else
	{
		shared_file_fp = fopen (shared_filename, "w") ;
        	if (shared_file_fp == NULL)
        	{
                	printf("Cannot open shared file...Server Exiting\n");
               		exit_server_flag=1;
			return;
        	}

		fprintf(shared_file_fp,"Unknown Opcode..Do Nothing!\n");
		fclose(shared_file_fp);
	}

}

static void Server_Handler(int sigNo)
{        	
	if(sigNo == SIGINT)
	{
		exit_server_flag=1;
	}

	server_flag = 1;
	sigprocmask(SIG_BLOCK, &newAction.sa_mask, &oldAction.sa_mask);	
        return;
}


int main()
{
	client_pid = getppid();
	
	//setup signal mask
	sigset_t zeromask;
	sigemptyset(&newAction.sa_mask);
	newAction.sa_flags = 0;
	newAction.sa_handler = Server_Handler;
	sigemptyset(&oldAction.sa_mask);
	sigemptyset(&zeromask);
	sigaddset(&newAction.sa_mask, SIGUSR1);
	sigaddset(&newAction.sa_mask, SIGINT);
	
	//installing signal handler for SIGUSR1
	if(sigaction(SIGUSR1, &newAction, &oldAction))
	{
		printf("SIGUSR1 Signal Handler not installed properly..Server Exiting.\n");
                exit(1);
	}
		
	//installing signal handler for SIGINT..for exiting the server if client is exiting
	if(sigaction(SIGINT, &newAction, &oldAction))
	{
		printf("SIGINT Signal Handler not installed properly..Server Exiting.\n");
                exit(1);
	}
                
	//blocking SIGUSR1 and SIGINT
	if(sigprocmask(SIG_BLOCK, &newAction.sa_mask, &oldAction.sa_mask))
	{
		printf("Signal SIGUSR1 and SIGINT not blocked properly..Server Exiting.\n");
                exit(1);
	}


	while(1)
	{
		while(server_flag == 0)
		{
			sigsuspend(&zeromask);
		}
		
		if(exit_server_flag==1)
		{
			break;
		}
		server_flag = 0;

		//Serve the user input
		PerformFileOperations();

		//In case of issue with the shared file
		if(exit_server_flag==1)
                {
                        break;
                }
		
		//Send the signal to the client
		if(kill(client_pid, SIGUSR1))
		{
			printf("Could not successfully send signal to client...");
			break;
		}
	}

	if(sigprocmask(SIG_SETMASK, &oldAction.sa_mask, NULL))
	{
		printf("Signals SIGUSR1 and SIGINT not unblocked properly\n");
	}

	printf("Server Exiting...\n");

	return 0;
}
