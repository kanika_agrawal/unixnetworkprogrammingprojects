#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

volatile sig_atomic_t client_flag = 0;
volatile sig_atomic_t shared_file_error_flag = 0;
struct sigaction newAction, oldAction;
sigset_t zeromask;

pid_t server_pid;

char *shared_filename = "/tmp/KA_shared_file.txt";

void DeleteSharedFile()
{
        int status;

        //remove will return an error if the file does not exist
        status = remove(shared_filename);

        if( status == 0 )
        {
                printf("Deleted shared file successfully.\n");
        }
        else
        {
                printf("Unable to delete the shared file \n");
        }
}

void OutputToUser()
{
	char ch;
   	FILE *shared_file_fp;
 
   	shared_file_fp = fopen(shared_filename,"r");
 
   	if( shared_file_fp == NULL )
   	{
		shared_file_error_flag = 1;
      		return;
   	}
   	while((ch = fgetc(shared_file_fp)) != EOF)
      	{
		printf("%c",ch);
	}
 
   	fclose(shared_file_fp);
}

static void Client_Handler(int sigNo)
{
	if(sigNo == SIGCHLD)
	{
		printf("Server crashed...Client Exiting\n");
        	_exit(1);
	}

	client_flag=1;
	sigprocmask(SIG_BLOCK, &newAction.sa_mask, &oldAction.sa_mask);
        return;
}

int main(void)
{
        char command[100];
	char opcode[10];

	int i = 0;
        
	//Fork the server
	if((server_pid = fork())< 0)
        {
        	printf("Fork Failed!\n");
        	exit(1);
        }
	if(server_pid == 0)
        {
        	execvp("./server", NULL);
       	}

	//set up signal mask
	sigemptyset(&newAction.sa_mask);
        newAction.sa_flags = 0;
        newAction.sa_handler = Client_Handler;
        sigemptyset(&oldAction.sa_mask);
        sigemptyset(&zeromask);
       	sigaddset(&newAction.sa_mask, SIGUSR1);
	sigaddset(&newAction.sa_mask, SIGCHLD);	

	//install signal handler for SIGUSR1
	if(sigaction(SIGUSR1, &newAction, &oldAction))
        {
		printf("SIGUSR1 Signal Handler not installed properly..Client Exiting...Sending SIGINT to server.\n");
		kill(server_pid, SIGINT);
		exit(1);
	}

	//install signal handler for SIGCHLD..in case of crashing of the server
	if(sigaction(SIGCHLD, &newAction, &oldAction))
        {
                printf("SIGCHLD Signal Handler not installed properly..Client Exiting...Sending SIGINT to server.\n");
                kill(server_pid, SIGINT);
                exit(1);
        }
        
	//blocking signals SIGCHLD and SIGUSR1
	if(sigprocmask(SIG_BLOCK, &newAction.sa_mask, &oldAction.sa_mask))
	{
		printf("Signal SIGUSR1 and SIGCHLD not blocked properly..Client Exiting...Sending SIGINT to server.\n");
                kill(server_pid, SIGINT);
                exit(1);
	}
	
	while(1)
        {
			//read ouser input
        		printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");
			
			bzero(command, 100);
			if(fgets(command, sizeof(command), stdin) == NULL)
			{
				printf("No command entered..please try again.\n");
				continue;
			}

			if(strlen(command) > 0)
			{
				command[strlen(command) - 1] = '\0';
			}

			else
			{
				printf("Please try again..\n");
				continue;
			}
		
		
			//Extract the opcode from the command
			while(command[i] != ' ' && command[i] != '\0') 
			{	
  				opcode[i] = command[i];
				i++;
			}
			opcode[i] = '\0';		
			i=0;
		
 			//Check if user wants to exit
			if(strcasecmp(opcode, "EXIT")== 0 )
                	{
                        	printf("Client Exiting..\n");
                        	break;
                	}
        		//Check if the opcode is correct
			else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
			{
				//Check if the filename is given or not
                		if(strlen(command)== strlen(opcode)|| strlen(command)==strlen(opcode)+1)
				{
                        		printf("Filename not entered\n");
                        		continue;
                		}

				//Open the shared file and write the opcode and the filename for the server to read
        			FILE  *shared_file_fp;
        			shared_file_fp = fopen(shared_filename,"w");
        			if (shared_file_fp == NULL)
        			{
               				printf("Cannot open shared file");
               				printf("Client Exiting...\n");
                        		break;
        			}
	
				fprintf(shared_file_fp, "%s\n", command);
        			fclose(shared_file_fp);
			}
			else
			{
				printf("Incorrect opcodes..please enter again\n");
                        	continue;
			}
	
        		//Signal the server
			if(kill(server_pid, SIGUSR1))
			{
				printf("Signal cannot be sent to the Server...Client Exiting");
				break;
			}
			
			//atomically sleep and unblock all the signals
			while(client_flag == 0)
			{
				sigsuspend (&zeromask);
			}
		
			client_flag = 0;

			//Printf the output to the user
			OutputToUser();

			if(shared_file_error_flag == 1)
			{
				printf("Error while opening the shared file.\n");
				break;
			}
	}

	if(sigprocmask(SIG_SETMASK, &oldAction.sa_mask, NULL))
	{
		printf("Old mask not reset properly.\n");
	}
	
	//Delete the shared file
	DeleteSharedFile();

	//Sending Signal SIGINT to the server;
        if(kill(server_pid, SIGINT))
	{
		printf("Interrupt Signal cannot be sent to the Server.");
	}
       	
	return 0;
}

