#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <semaphore.h>

key_t SHM_KEY = 0611;
key_t SEM_KEY = 06111;

#define SHM_PATH "/kanika1234"
#define SEM_CLIENT "kanika_1234"
#define SEM_SERVER "kanika_2345"

#define SHM_SIZE 1024

#define FILE_READ 0
#define FILE_DELETE 1
#define SERVER_RESPONSE 2
#define EXIT 3

#define USE_PSM 1
#define USE_SSM 2


volatile sig_atomic_t exit_server_flag =0;

pid_t server_pid = -1;

typedef struct
{
    int mesg_len;
    int mesg_type;
    char mesg_data[SHM_SIZE];
} Mesg;


Mesg getUserInputRequest()
{
    char command[500];
    char opcode[10];
    char filename[400];
    Mesg msg;
    
    while(1)
    {    
        int i=0;
        
        //read user input
	bzero(command, 100);
        bzero(opcode, 10);
        bzero(filename, 50);
        
        printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");
     
        if(fgets(command, sizeof(command), stdin) == NULL)
        {
            printf("No command entered..please try again.\n");
            continue;
        }

        if(strlen(command) > 0)
        {
            command[strlen(command) - 1] = '\0';
        
        }
        else
        {
            printf("Please try again..\n");
            continue;
        }

        //Extract the opcode
        while(command[i] != ' ' && command[i] != '\0') 
        {	
            opcode[i] = command[i];
            i++;
        }

        opcode[i] = '\0';	

        //Check is user wants to exit
        if(strcasecmp(opcode, "EXIT")== 0 )
        {
            msg.mesg_type = EXIT;
            return msg;
        }

        //Check if the opcode is correct or not
        else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
        {
            int j=0;
            //Remove white spaces
            while(command[i] == ' ')
                    i++;

            //Copy filename form the command
            while(command[i]!='\0')
            {
                    filename[j] = command[i];
                    i++;
                    j++;
            }
            filename[j] = '\0';

            //Check if the filename is given or not
            if(strlen(filename)==0)
            {
                    printf("Filename not entered\n");
                    continue;
            }

            //update the message		
            msg.mesg_len = strlen(filename);
            strcpy(msg.mesg_data ,filename);

            if(strcasecmp(opcode, "READ") == 0)
            {
                    msg.mesg_type = FILE_READ;
            }
            else
            {
                    msg.mesg_type = FILE_DELETE;
            }

            return msg;
        }
        
        //incorrect opcode
        else
        {
            printf("Incorrect opcodes..please enter again\n");
            continue;
        }
    }	
}

void PerformSystemVCleanups(int shmfd, int semid)
{
	//if server has been forked
	if(server_pid != -1)
	{
		printf("Sending SIGINT to the Server.\n");
        	if(kill(server_pid, SIGINT))
        	{
        		printf("Interrupt Signal cannot be sent to the Server.");
        	}
	}

        //Unlink the shared memory
        if (shmctl(shmfd, IPC_RMID, NULL) == -1)
        {
        	printf("Cannot unlink the System V Shared memory..program exiting.\n");
        }

        //Unlink the semaphores
        if(semctl(semid, 2, IPC_RMID) == -1)
        {
        	printf("Cannot delete the System V Sempahore.\n");
        }
}

void PerformPosixCleanups()
{
	 //if server has been forked
         if(server_pid != -1)
	 {
		printf("Sending SIGINT to the Server.\n");
         	if(kill(server_pid, SIGINT))
         	{
         		printf("Interrupt Signal cannot be sent to the Server.");
         	}
	 }

         //Unlink the CLient Semaphore
         if(sem_unlink(SEM_CLIENT) < 0)
         {
         	printf("Cannot delete the Client Semaphore.\n");
         }

         //Unlink the Server Semaphore
         if(sem_unlink(SEM_SERVER) < 0)
         {
         	printf("Cannot delete the Server Semaphore.\n");
         }

         //unlink the Posix Shared memory
         if (shm_unlink(SHM_PATH) != 0)
         {
         	printf("Cannot unlink the Posix Shared Memory.\n");
         }

}

void unlink_Posix_Client_Sempahore()
{
	if(shm_unlink(SEM_CLIENT) < 0)
        {
        	printf("Cannot delete the Client Semaphore.\n");
       	}
}

void unlink_Posix_Shared_Memory()
{
	 if (shm_unlink(SHM_PATH) != 0)
         {
                printf("Cannot unlink the Posix Shared Memory.\n");
         }
}

void termination_handler (int signum)
{
    exit_server_flag = 1;
}

void client_termination(int signum)
{
	printf("Server Crashed...Client Exiting.\n");
	_exit(1); 
}

int main(int argc, char *argv[])
{
	signal(SIGCHLD, client_termination);

	int ipc_mode = USE_PSM;
	char *ipc = "PSM";
	
	if (argc != 2 )
    	{
        	printf("IPC Mode not entered...Default IPC: Posix Shared Memory is used\n");	
    	}
    	else 
    	{
		ipc = argv[1];	
	}

	if(strcasecmp(ipc, "PSM")== 0)
		ipc_mode = USE_PSM;
	
	else if(strcasecmp(ipc, "SSM")== 0)
		ipc_mode = USE_SSM;

	else
		ipc_mode = USE_PSM;
 
	switch(ipc_mode) 
	{
	    case USE_PSM:
	    {
       		//Delete if shared memory already exists
        	int shmfd;
        	if(shm_open(SHM_PATH, O_RDWR, S_IRWXU | S_IRWXG) >= 0)
        	{
                	printf("Rogue Posix Shared Memory already exists..unlinking it.\n");
                	
			if (shm_unlink(SHM_PATH) != 0)
                	{
                        	printf("Cannot unlink the rogue Posix Shared memory..program exiting.\n");
				exit(1);
                	}
        	}

		//Create the new shared memory
        	shmfd = shm_open(SHM_PATH, O_CREAT | O_EXCL | O_RDWR, S_IRWXU | S_IRWXG);
        	if(shmfd < 0)
        	{
                	printf("Cannot create the Posix shared memory...program exiting.");
                	exit(1);
        	}

        	if(ftruncate(shmfd, SHM_SIZE)<0)
        	{
                	printf("Cannot truncate the shared memory to the specified file size. Continuing.\n");
        	}

        	//Create the Posix Semaphore
        	sem_t *client_sem, *server_sem;

        	//Client Semaphore
        	client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 1);
        	if(client_sem == SEM_FAILED)
        	{
                	if(errno == EEXIST)
                	{
                        	printf("Client Semaphore already exists..deleting it.\n");
                        	if(sem_unlink(SEM_CLIENT)< 0 )
				{
					printf("Cannot delete the rogue Client Semaphore..Deleting Shared Memory...program exiting.\n");
					unlink_Posix_Shared_Memory();
					exit(1);	
				}

                        	client_sem = sem_open(SEM_CLIENT, O_CREAT | O_EXCL | O_RDWR, 0777, 1);
                        	if(client_sem == SEM_FAILED)
                        	{
                                	printf("Cannot create Client Semaphore...Deleting Shared Memory...program exiting.\n");
                                	unlink_Posix_Shared_Memory();
                                	exit(1);
                        	}
                	}
			else
			{
				printf("Error in creating the Client Sempahore..Deleting Shared Memory..Program Exiting..\n");
				unlink_Posix_Shared_Memory();
                                exit(1);
			}
			 
          	}

		//Server Semaphore
        	server_sem = sem_open(SEM_SERVER, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
        	if(server_sem == SEM_FAILED)
        	{
                	if(errno == EEXIST)
                	{
                        	printf("Server Semaphore already exists..deleting it.\n");
                        	if(sem_unlink(SEM_SERVER) < 0)
				{
					printf("Cannot create Server Semaphore...Deleting Client Semaphore and Unlinking Posix Shared Memory...program exiting.\n");
					unlink_Posix_Client_Sempahore();
					unlink_Posix_Shared_Memory();
                                        exit(1);
				}

				//Create the Server Semaphore after deleting the rogue one
                        	server_sem = sem_open(SEM_SERVER, O_CREAT | O_EXCL | O_RDWR, 0777, 0);
                        	if(server_sem == SEM_FAILED)
                        	{
                                	printf("Cannot create Server Semaphore...Deleting Client Semaphore and Unlinking Posix Shared Memory...program exiting.\n");
					unlink_Posix_Client_Sempahore();
                                        unlink_Posix_Shared_Memory();
                                	exit(1);
                        	}
                	}

			else
                        {       
                       		printf("Error in creating the Server Sempahore..Deleting Client Semaphore and deleting Shared Memory..Program Exiting..\n");
                                unlink_Posix_Client_Sempahore();
                                unlink_Posix_Shared_Memory();
                                exit(1);
                    	}
        	}

        	//Fork the server
        	if((server_pid = fork())< 0)
        	{
                	printf("Fork Failed!\n");
			PerformPosixCleanups();
                	exit(1);
        	}
        	if(server_pid == 0)
        	{
                	execvp("./server_posix", NULL);
        	}

        	//Attach the shared memory segment
        	char *shm;
        	shm = mmap(NULL, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shmfd, 0);
        	if(shm == NULL)
        	{
                	printf("Error in attaching the shared memory with client...program exiting..\n");
			PerformPosixCleanups();
			exit(1);
        	}

        	//take the lock
        	if(sem_wait(client_sem) < 0)
		{
			printf("Error in taking the lock on the client side...Client Exiting.\n");
			PerformPosixCleanups();
			exit(1);
		}
		
		while(1)
                {
                        Mesg msgToSend = getUserInputRequest();

                        if(msgToSend.mesg_type == EXIT)
                        {
                            break;
                        }

                        char command[SHM_SIZE];
                        bzero(command, SHM_SIZE);

                        if(msgToSend.mesg_type == FILE_READ)
                        {
                                strcpy(command, "Read ");
                                strcat(command, msgToSend.mesg_data);
                        }
                        else
                        {
                                strcpy(command, "Delete ");
                                strcat(command, msgToSend.mesg_data);
                        }

                        //Write to the server
                        strcpy(shm, command);

                        //Signal the server
                        if(sem_post(server_sem) < 0 )
			{
				printf("Error in siganlling the server.\n");
				break;
			}

                        //Wait for the lock for reading server's response
                        if(sem_wait(client_sem) < 0)
			{
				printf("Error in taking the lock on the client side.\n");
				break;
			}

                        //Print the output to the user
                        printf("Client Reads:\n");
                        char *str;
                        for (str = shm; *str != '\0'; str++)
                                putchar(*str);
                        putchar('\n');
                }

		PerformPosixCleanups();

		printf("Client Exiting...\n");
		break;
	    }

            case USE_SSM:
            {       
		//Check if the rogue shared memory already exists in the system
        	int shmfd;

		shmfd = shmget(SHM_KEY, SHM_SIZE, 0777);
        	if(shmfd >= 0)
        	{
                	printf("Rogue System V Shared Memory already exists..unlinking it.\n");
                	if (shmctl(shmfd, IPC_RMID, NULL) == -1)
                	{
                                printf("Cannot unlink the rogue System V Shared memory..program exiting.\n");
                                exit(1);
                	}
        	}

        	//Create the System v Shared Memory Segment
        	shmfd = shmget(SHM_KEY, SHM_SIZE, 0777 | IPC_CREAT);
        	if(shmfd < 0)
        	{
                	printf("Cannot create the System V shared memory..program exiting.");
                	exit(1);
        	}

        	//Create the System V Semaphore
        	int semid;
        	unsigned short semval;
        	struct sembuf wait,signal;

        	wait.sem_num = 1;
        	wait.sem_op = -1;
        	wait.sem_flg = SEM_UNDO;

        	signal.sem_num = 0;
        	signal.sem_op = 1;
        	signal.sem_flg = SEM_UNDO;

        	union semun
        	{
                	int val;
                	struct semid_ds *buf;
                	ushort * array;
        	} arg1, arg2;

        	arg1.val = 0;
        	arg2.val = 1;

        	//Check if the rogue semaphores already exist
        	semid = semget(SEM_KEY, 2 ,0777);
        	if(semid >= 0)
        	{
                	printf("System V Semaphore already exists..deleting it.\n");
                	if (semctl(semid, 2, IPC_RMID) == -1)
                	{
                        	printf("Cannot delete the rogue System V Sempahore...unlinking the shared memory...program exiting.\n");
                        	if (shmctl(shmfd, IPC_RMID, NULL) == -1)
                        	{
                                	printf("Cannot unlink the System V Shared memory..\n");
                        	}
                        	exit(1);
                	}
        	}
		
		//Create the new sempahore
        	semid = semget(SEM_KEY,2, 0777 | IPC_CREAT);
        	if(semid < 0)
        	{
                	printf("Cannot create the System V semaphores...unlinking System Shared Memory..program exiting.\n");
		 	if (shmctl(shmfd, IPC_RMID, NULL) == -1)
                        {
                        	printf("Cannot unlink the System V Shared memory..\n");
                        }
                	exit(1);
        	}

        	//Set the value of server semaphore to be 0
        	if(semctl(semid, 0 ,SETVAL, arg1) < 0)
        	{
                	printf("Cannot set arg1 semaphore value.\n");
			PerformSystemVCleanups(shmfd, semid);
                	exit(1);
        	}

        	//Set the value of the client semaphore to be 1
        	if(semctl(semid ,1, SETVAL, arg2) < 0)
        	{
                	printf("Cannot set arg2 semaphore value.\n");
			PerformSystemVCleanups(shmfd, semid);
                	exit(1);
        	}

        	//Fork the server
        	if((server_pid = fork())< 0)
        	{
                	printf("Fork Failed!\n");
			PerformSystemVCleanups(shmfd, semid);
                	exit(1);
        	}
        	if(server_pid == 0)
        	{
                	execvp("./server_sys", NULL);
        	}

        	//Attach the shared memory segment
        	char *shm;
        	if ((shm = shmat(shmfd, NULL, 0)) == (char *) -1)
        	{
                	printf("Error in attaching shared memory with the Client..program exiting.\n");
			PerformSystemVCleanups(shmfd,semid);
                	exit(1);
        	}

        	//Take the lock
        	if(semop(semid, &wait, 1) < 0)
		{
			printf("Error in taking the lock at the client sempahore..program exiting.\n");
                        PerformSystemVCleanups(shmfd,semid);
                        exit(1);
		}

		while(1)
                {
                        Mesg msgToSend = getUserInputRequest();

                        if(msgToSend.mesg_type == EXIT)
                        {
                            break;
                        }
			
			char command[SHM_SIZE];
			bzero(command, SHM_SIZE);
                     
			if(msgToSend.mesg_type == FILE_READ)
			{
				strcpy(command, "Read ");
				strcat(command, msgToSend.mesg_data);
			}
			else
			{
				strcpy(command, "Delete ");
                                strcat(command, msgToSend.mesg_data);
			}

			//Write to the server
                        strcpy(shm, command);

			//Signal the server
                	if(semop(semid,&signal,1) < 0)
			{
				printf("Error in signalling the server on the server semaphore..program exiting.\n");
				break;
			}

                	//Wait for the lock for reading server's response
                	if(semop(semid, &wait, 1)< 0)
			{
				printf("Error in taking the lock on the client semaphore..program exiting.\n");
                                break;
			}

                	//Print the output to the user
                	printf("Client Reads:\n");
                	char *str;
                	for (str = shm; *str !='\0'; str++)
                        	putchar(*str);
                	putchar('\n');
                }
		
		PerformSystemVCleanups(shmfd, semid);
		printf("Client Exiting..\n");
               	break;
	    }
                
            default:
            {
                printf("invalid IPC selected. Terminating\n");
                break;
            }
	}
	
        return 0;
}

