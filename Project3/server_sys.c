#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>

key_t SHM_KEY = 0611;
key_t SEM_KEY= 06111;

#define SHM_SIZE 1024

char* ReadFile(char filename[1024])
{
        FILE *file_to_read_fp;
        char ch;

	char *msg  = malloc(SHM_SIZE * sizeof(char));
	bzero(msg, SHM_SIZE);
	
	//Check if the given path is a file or directory
	struct stat s;
	if(stat(filename, &s) == 0)
	{
		if(!(s.st_mode & S_IFREG))
		{
			sprintf(msg,"Cannot read the file [%s] as path refers to the directory.\n", filename);
			return msg;
		}
	}
	
	//Check if the file to be read from exists
 	int rval = access (filename, F_OK);
 	if (rval != 0) 
	{
 		if (errno == ENOENT) 
   			sprintf(msg,"Cannot read the file [%s] as it does not exist\n", filename);
  		else if (errno == EACCES) 
   			sprintf(msg,"Cannot read the file [%s] as it is not accessible\n", filename);
  		return msg;
 	}

 	//Check read accessof the file to be read from 
 	rval = access (filename, R_OK);
 	if (rval != 0)
  	{
  		sprintf(msg,"Access denied to the file [%s].\n", filename);		
		return msg;
	}

	//Open the file to be read
	file_to_read_fp = fopen(filename, "r");
        if (file_to_read_fp == NULL)
        {
                sprintf(msg,"Cannot open the file to read from.\n");
		return msg;
        }

        
	fread(msg, 1, SHM_SIZE, file_to_read_fp);
        fclose(file_to_read_fp);
	return msg;
}

char* DeleteFile(char filename[1024])
{	
	//delete the file  
	char *msg  = malloc(SHM_SIZE * sizeof(char));
        bzero(msg, SHM_SIZE);

	int status = unlink(filename);
        if( status == 0 )
        {
	        sprintf(msg,"file [%s] deleted successfully.\n",filename);
	}
	else
	{
		if(errno  == EISDIR)
		{
			sprintf(msg,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
		}
		else if(errno  == EACCES)
		{
			sprintf(msg,"Cannot delete the file [%s] as it is not accessible.\n", filename);
		}
		else if(errno == ENOENT)
		{
			sprintf(msg,"Cannot find the file [%s] for deletion. \n", filename);
		}
        	else
        	{
                	sprintf(msg,"Unable to delete the file [%s].\n", filename);
        	}
	}

        return msg;
}

char* PerformFileOperations(char *data)
{
        char opcode[10];
        char filename[1024];
	int i = 0;
	int j = 0;
	
	char *msg;
	// cleaning up char arrays
	bzero(opcode, 10);
	bzero(filename, 1024);

	// read opcode
        while(data[i] != ' ' && data[i] != '\0')
        {
            opcode[i] = data[i];
            i++;
        }
        opcode[i] = '\0';

	//remove white spaces from the middle
	while(data[i] == ' ')
        {
		i++;
	}

	//read filename
	while(data[i]!='\0')
        {
              filename[j] = data[i];
              i++;
              j++;
       	}
        filename[j] = '\0';

	//Call appropriate functions according to the opcode
        if(strcasecmp(opcode, "READ")==0)
        {
                msg  = ReadFile(filename);
        }
        else if(strcasecmp(opcode,"DELETE")== 0)
        {
                 msg = DeleteFile(filename);
        }
	else
	{
		sprintf(msg, "Unknown opcode..%s\n", opcode);
	}
	return msg;
}

static void Server_Handler(int sigNo)
{        	
	if(sigNo == SIGINT)
	{
		printf("Server Exiting...\n");
		_exit(1);
	}
        return;
}


int main()
{
	char *data;
        char *shm_server;

	int shmfd;
	shmfd = shmget(SHM_KEY, SHM_SIZE, 0666);        
	if(shmfd < 0)
        {
        	printf("Server:Cannot get the key to the System V Shared Memory...server exiting.");
         	exit(1);
        }

	int semid;
        unsigned short semval;
        struct sembuf wait,signal;

        wait.sem_num = 0;
        wait.sem_op = -1;
        wait.sem_flg = SEM_UNDO;

        signal.sem_num = 1;
        signal.sem_op = 1;
        signal.sem_flg = SEM_UNDO;	

	semid = semget(SEM_KEY, 2 ,0777);
        if(semid < 0)
        {
 	       	printf("Cannot get the key to the System V Sempahore...server exiting.");
		exit(1);
        }
	
	/*Now we attach the segment to our data space*/
        if ((shm_server = shmat(shmfd, NULL, 0)) == (char *) -1)
        {
        	printf("Cannot attach to the shared memory...server exiting.\n");
                exit(1);
        }

	while(1)
	{
		char *msgToSend = NULL, *str = NULL;
		int  i = 0;
		data  = (char*)calloc(SHM_SIZE,sizeof(char));
		
		//Wait for the lock
		if(semop(semid, &wait, 1) < 0)
		{
			printf("Erron in taking the lock on the server side.\n");
			break;
		}

		/*Now read what the client put in the memory*/
                for( i = 0; shm_server[i] != '\0'; i++)
                {      
			 data[i] = shm_server[i];
		}
		
		//Serve the user input
		msgToSend = PerformFileOperations(data);	
		
		//Wrte the results into the memory
		strncpy(shm_server, msgToSend, SHM_SIZE);
		
		free(data);
		free(msgToSend);

		//Signal the client
		if(semop(semid,&signal,1) < 0)
		{
			printf("Error in signalling the client.\n");
			break;
		}
	}

	printf("Server Exiting...\n");

	return 0;
}
