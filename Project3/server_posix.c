#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/mman.h>
#include <semaphore.h>

#define SHM_PATH "/kanika1234"
#define SEM_CLIENT "kanika_1234"
#define SEM_SERVER "kanika_2345"

#define SHM_SIZE 1024

char* ReadFile(char filename[1024])
{
        FILE *file_to_read_fp;
        char ch;

	char *msg = malloc(SHM_SIZE * sizeof(char));
	bzero(msg, SHM_SIZE);
	
	//Check if the given path is a file or directory
	struct stat s;
	if(stat(filename, &s) == 0)
	{
		if(!(s.st_mode & S_IFREG))
		{
			sprintf(msg,"Cannot read the file [%s] as path refers to the directory.\n", filename);
			return msg;
		}
	}
	
	//Check if the file to be read from exists
 	int rval = access (filename, F_OK);
 	if (rval != 0) 
	{
 		if (errno == ENOENT) 
   			sprintf(msg,"Cannot read the file [%s] as it does not exist\n", filename);
  		else if (errno == EACCES) 
   			sprintf(msg,"Cannot read the file [%s] as it is not accessible\n", filename);
  		return msg;
 	}

 	//Check read accessof the file to be read from 
 	rval = access (filename, R_OK);
 	if (rval != 0)
  	{
  		sprintf(msg,"Access denied to the file [%s].\n", filename);		
		return msg;
	}

	//Open the file to be read
	file_to_read_fp = fopen(filename, "r");
        if (file_to_read_fp == NULL)
        {
                sprintf(msg,"Cannot open the file to read from.\n");
		return msg;
        }

        
	fread(msg, 1, SHM_SIZE, file_to_read_fp);
        fclose(file_to_read_fp);
	return msg;
}

char* DeleteFile(char filename[1024])
{	
	//delete the file  
	char *msg = malloc(SHM_SIZE * sizeof(char));
        bzero(msg, SHM_SIZE);

	int status = unlink(filename);
        if( status == 0 )
        {
	        sprintf(msg,"file [%s] deleted successfully.\n",filename);
	}
	else
	{
		if(errno  == EISDIR)
		{
			sprintf(msg,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
		}
		else if(errno  == EACCES)
		{
			sprintf(msg,"Cannot delete the file [%s] as it is not accessible.\n", filename);
		}
		else if(errno == ENOENT)
		{
			sprintf(msg,"Cannot find the file [%s] for deletion. \n", filename);
		}
        	else
        	{
                	sprintf(msg,"Unable to delete the file [%s].\n", filename);
        	}
	}

        return msg;
}

char* PerformFileOperations(char *data)
{
        char opcode[10];
        char filename[1024];
	int i = 0;
	int j = 0;
	
	char *msg;
	// cleaning up char arrays
	bzero(opcode, 10);
	bzero(filename, 1024);

	// read opcode
        while(data[i] != ' ' && data[i] != '\0')
        {
            opcode[i] = data[i];
            i++;
        }
        opcode[i] = '\0';

	//remove white spaces from the middle
	while(data[i] == ' ')
        {
		i++;
	}

	//read filename
	while(data[i]!='\0')
        {
              filename[j] = data[i];
              i++;
              j++;
       	}
        filename[j] = '\0';

	//Call appropriate functions according to the opcode
        if(strcasecmp(opcode, "READ")==0)
        {
                msg  = ReadFile(filename);
        }
        else if(strcasecmp(opcode,"DELETE")== 0)
        {
                 msg = DeleteFile(filename);
        }
	else
	{
		sprintf(msg, "Unknown opcode..%s\n", opcode);
	}
	return msg;
}

static void Server_Handler(int sigNo)
{        	
	if(sigNo == SIGINT)
	{
		printf("Server Exiting.\n");
		_exit(1);
	}
}


int main()
{	
	char *data;
        char *shm_server;

	//Open the shared memory segment on the server seide
	int shmfd;
	shmfd = shm_open(SHM_PATH, O_RDWR, S_IRWXU | S_IRWXG);
        if(shmfd < 0)
        {
                printf("Server: Cannot open the Posix shared memory...program exiting.");
                exit(1);
        }
	
	/*Now we attach the segment to our data space*/
        shm_server = mmap(NULL, SHM_SIZE, PROT_READ| PROT_WRITE, MAP_SHARED, shmfd, 0);
        if(shm_server == NULL)
        {
                printf("Error in Server while attching to the Shared Memory...Server Exiting..\n");
                exit(1);
        }


	//Open the shared Posix semaphores
	sem_t *client_sem, *server_sem;

        client_sem = sem_open(SEM_CLIENT,0);
	if(client_sem  ==  SEM_FAILED)
	{
		printf("Cannot open the Client Semaphore..server exiting.\n");
		exit(1);
	}

        server_sem = sem_open(SEM_SERVER,0);
	if(server_sem == SEM_FAILED)
	{
		printf("Cannot open the Server Semaphore..server exiting.\n");
                exit(1);		
	}

	while(1)
	{
		char *msgToSend = NULL, *str = NULL;
		int  i = 0;
		data  = (char*)calloc(SHM_SIZE,sizeof(char));
	
		if(sem_wait(server_sem) < 0)
		{
			printf("Error in taking lock on the server side.\n");
			break;
		}

		/*Now read what the client put in the memory*/
                for( i = 0; shm_server[i] != '\0'; i++)
                {      
			 data[i] = shm_server[i];
		}

		//Serve the user input
		msgToSend = PerformFileOperations(data);	
		
		//Wrte the results into the memory
		strncpy(shm_server, msgToSend, SHM_SIZE);
		
		free(data);
		free(msgToSend);
		
		//Signal the client
		if(sem_post(client_sem) < 0)
		{
			printf("Error in signalling the client.\n");
                        break;
		}
	}

	printf("Server Exiting...\n");

	return 0;
}
