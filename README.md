Designed and implemented Client-Server IPC using

1. POSIX reliable signals ensuring that none of the signals were lost during the IPC.
1. Pipes, FIFOs, POSIX Message Queues, System V Message Queues.
1. System V Shared Memory with used System V Semaphores for synchronization and POSIX Shared Memory with POSIX Semaphores for synchronization.
1. Sockets with both connection less (UDP) and connection oriented (TCP) protocol.