#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ipc.h>
#include <mqueue.h>

#define FILE_READ 0
#define FILE_DELETE 1
#define SERVER_RESPONSE 2
#define EXIT 3

#define USE_PIPE 1
#define USE_FIFO 2
#define USE_POMQ 3
#define USE_SVMQ 4

#define FIFO1 "/tmp/fifokanika1"
#define FIFO2 "/tmp/fifokanika2"

#define MQNAME "/server_1"
#define MQNAME_2 "/client_1"

#define MQ_KEY1 "1234L"
#define MQ_KEY2 "2345L"

#define MAXBUF 1024
#define POMQ_BUFSIZE 2050
#define POMQ_MSGSIZE 2048

volatile sig_atomic_t exit_server_flag =0;

pid_t server_pid;

typedef struct
{
    int mesg_len;
    int mesg_type;
    char mesg_data[MAXBUF];
} Mesg;

Mesg ReadFile(char filename[100])
{
    Mesg msg;
    msg.mesg_type = SERVER_RESPONSE;

    bzero(msg.mesg_data, MAXBUF);
    
     //Check if the given path is a file or directory
     struct stat s;
     if(stat(filename, &s) == 0)
     {
      	if(!(s.st_mode & S_IFREG))
        {
            sprintf(msg.mesg_data,"Cannot read the file [%s] as path refers to the directory.\n", filename);
	    msg.mesg_len = strlen(msg.mesg_data);
	    return msg;
        }
     }

    //Check if the file to be read from exists
    int rval = access (filename, F_OK);
    if (rval != 0)
    {
        if (errno == ENOENT)
        {
            sprintf(msg.mesg_data, "Cannot read the file [%s] as it does not exist.", filename);
        }
        else if (errno == EACCES)
        {
            sprintf(msg.mesg_data, "Cannot read the file [%s] as it is not accessible.", filename);
        }

        msg.mesg_len = strlen(msg.mesg_data);		

        return msg;
    }

    //Check read access of the file to be read from 
    rval = access (filename, R_OK);
    if (rval != 0)
    {	
        sprintf(msg.mesg_data,"Access denied to the file [%s].", filename);
        msg.mesg_len = strlen(msg.mesg_data);
        return msg;
    }

    //Open the file to be read
    FILE *file_to_read_fp = fopen(filename, "r");
    if (file_to_read_fp == NULL)
    {
        sprintf(msg.mesg_data,"Cannot open the file to read from.");
        msg.mesg_len = strlen(msg.mesg_data);
        return msg;
    }

    fread(msg.mesg_data, 1, MAXBUF, file_to_read_fp);
    msg.mesg_len = strlen(msg.mesg_data);

    fclose(file_to_read_fp);
    return msg;
}

Mesg DeleteFile(char filename[100])
{
    Mesg msg;
    msg.mesg_type = SERVER_RESPONSE;

    //delete the file
    int status = unlink(filename);
    if( status == 0 )
    {
        sprintf(msg.mesg_data,"file [%s] deleted successfully.\n",filename);
    }
    else
    {
        if(errno  == EISDIR)
        {
            sprintf(msg.mesg_data,"Cannot delete the file [%s] as path refers to a directory.\n", filename);
        }
        else if(errno  == EACCES)
        {
            sprintf(msg.mesg_data,"Cannot delete the file [%s] as it is not accessible.\n", filename);
        }
        else if(errno == ENOENT)
        {
            sprintf(msg.mesg_data,"Cannot find the file [%s] for deletion. \n", filename);
        }
        else
        {
            sprintf(msg.mesg_data,"Unable to delete the file [%s].\n", filename);
        }
    }
    msg.mesg_len = strlen(msg.mesg_data);
    return msg;
}

Mesg Perform_File_Operations(int msg_type, char filename[100])
{
    switch(msg_type)
    {
        case FILE_READ:
        { 
            Mesg msg = ReadFile(filename);
            return msg;
            break;
        }
        case FILE_DELETE:
        {
            Mesg msg = DeleteFile(filename); 
            return msg;
            break;
        }

        default: 
        {
            Mesg msg;
            msg.mesg_type = SERVER_RESPONSE;
            sprintf(msg.mesg_data,"Invalid Opcode");
            msg.mesg_len = strlen(msg.mesg_data);
            return msg;
            break;
        }
    }
}

void server_pipe(int read_fd, int write_fd) 
{
    int n;
    char filename[1025];
    Mesg message;
	
    while(1)
    {
	if(exit_server_flag == 1)
		break;
        
	//server reads from the client
        if ((n = read(read_fd, &message, sizeof(Mesg))) >= 0)
        {
            Mesg msg = Perform_File_Operations(message.mesg_type, message.mesg_data);
            
	    //Send the response to the client
	    if(write(write_fd,&msg, sizeof(Mesg)) < 0)
	    {
		printf ("Error in sending response to the client in Pipes..Server Exiting...\n");
                exit (1);
            }
        }
	else
	{
		printf ("Error in receiving request from the client in Pipes..Server Exiting...\n");
                exit (1);
	}
    }   
    printf("Server Exiting...\n");                                                                       
}


void server_fifo(int read_fd, int write_fd)
{
       	int n;
        char filename[1025];      
	Mesg message;
	
	while(1)
	{
	    if(exit_server_flag == 1)
                break;
            
	    //server reads from the client
            if ((n = read(read_fd, &message, sizeof(Mesg))) >= 0)
            {
                Mesg msg = Perform_File_Operations(message.mesg_type, message.mesg_data);
                
		//Send response to the client
		if(write(write_fd, &msg, sizeof(Mesg)) < 0)
		{
			printf ("Error in sending response to the client in FIFO..Server Exiting...\n");
                	exit(1);
		}
            }
	    else
	    {
		printf ("Error in receiving request from the client in FIFO..Server Exiting...\n");
                exit(1);
	    }	
	}
        printf("Server Exiting..\n");
}


void server_system_v_mq(int read_fd, int write_fd)
{
        int n;
        char filename[1025];
        Mesg message;

        while(1)
        {
	    if(exit_server_flag == 1)
                break;
            
	    //server reads from the client
            if ((msgrcv(read_fd, &message, sizeof(Mesg),0,0)) == -1)
            {
                if(exit_server_flag == 0)
                   printf ("Error in receiving response from the client in System V Message Queues..Server Exiting...\n");

                exit (1);
            }

	    Mesg msg = Perform_File_Operations(message.mesg_type, message.mesg_data);
            
	    if(msgsnd(write_fd, &msg, sizeof(Mesg),0)== -1)
	    {
                    if(exit_server_flag == 0)
                        printf ("Error in sending response to the client in System V Message Queues..Server Exiting...\n");
                    
                    exit (1);
            }
        }
        printf("Server Exiting..\n");
}


void server_POMQ(mqd_t read_fd, mqd_t write_fd)
{
        int n;
        char filename[1025];
        Mesg msg;

        while(1)
        {
	    if(exit_server_flag == 1)
                break;
            
	    //server reads from the client
            if(mq_receive (read_fd, (char *)&msg, POMQ_BUFSIZE, NULL) < 0)
            {
                if(exit_server_flag == 0)
                    printf ("Error in receiving response from the client in Posix Message Queues..Server Exiting...\n");
                
                exit(1);
            }

            Mesg mesg = Perform_File_Operations(msg.mesg_type, msg.mesg_data);
                
	    if(mq_send (write_fd, (char *)&mesg, sizeof(Mesg), 1) < 0)
            {
                    if(exit_server_flag == 0)
                        printf ("Error in sending response to the client in Posix Message Queues..Server Exiting...\n");
                    
                    exit (1);
            }   
        }
        printf("Server Exiting..\n");
}

Mesg getUserInputRequest()
{
    char command[500];
    char opcode[10];
    char filename[400];
    Mesg msg;
    
    while(1)
    {    
        int i=0;
        
        //read user input
	bzero(command, 100);
        bzero(opcode, 10);
        bzero(filename, 50);
        
        printf("Please enter the command <opcode filename>.. Opcode = Read, Delete. For exiting..please enter Exit\n");
     
        if(fgets(command, sizeof(command), stdin) == NULL)
        {
            printf("No command entered..please try again.\n");
            continue;
        }

        if(strlen(command) > 0)
        {
            command[strlen(command) - 1] = '\0';
        
        }
        else
        {
            printf("Please try again..\n");
            continue;
        }

        //Extract the opcode
        while(command[i] != ' ' && command[i] != '\0') 
        {	
            opcode[i] = command[i];
            i++;
        }

        opcode[i] = '\0';	

        //Check is user wants to exit
        if(strcasecmp(opcode, "EXIT")== 0 )
        {
            msg.mesg_type = EXIT;
            return msg;
        }

        //Check if the opcode is correct or not
        else if((strcasecmp(opcode, "READ") == 0) || (strcasecmp(opcode,"DELETE")==0))
        {
            int j=0;
            //Remove white spaces
            while(command[i] == ' ')
                    i++;

            //Copy filename form the command
            while(command[i]!='\0')
            {
                    filename[j] = command[i];
                    i++;
                    j++;
            }
            filename[j] = '\0';

            //Check if the filename is given or not
            if(strlen(filename)==0)
            {
                    printf("Filename not entered\n");
                    continue;
            }

            //update the message		
            msg.mesg_len = strlen(filename);
            strcpy(msg.mesg_data ,filename);

            if(strcasecmp(opcode, "READ") == 0)
            {
                    msg.mesg_type = FILE_READ;
            }
            else
            {
                    msg.mesg_type = FILE_DELETE;
            }

            return msg;
        }
        
        //incorrect opcode
        else
        {
            printf("Incorrect opcodes..please enter again\n");
            continue;
        }
    }	
}

void termination_handler (int signum)
{
    exit_server_flag = 1;
}

void client_termination(int signum)
{
	printf("Server Crashed...Client Exiting.\n");
	_exit(1); 
}

int main(int argc, char *argv[])
{
	signal(SIGCHLD, client_termination);

	int ipc_mode = USE_PIPE;
	char *ipc = "PIPE";
	
	if (argc != 2 )
    	{
        	printf("IPC Mode not entered...Default IPC: PIPES is used\n");	
    	}
    	else 
    	{
		ipc = argv[1];	
	}

	if(strcasecmp(ipc, "PIPE")== 0)
		ipc_mode = USE_PIPE;
	
	else if(strcasecmp(ipc, "FIFO")== 0)
		ipc_mode = USE_FIFO;

	else if(strcasecmp(ipc, "POMQ")== 0)
                ipc_mode = USE_POMQ;

	else if(strcasecmp(ipc, "SVMQ")== 0)
                ipc_mode = USE_SVMQ;

	else
		ipc_mode  = 0;


	switch(ipc_mode)
	{
            case USE_PIPE:
            {       
                int pipe_1[2];
                int pipe_2[2];

                //Create Pipes
                if(pipe(pipe_1) < 0)
		{
			printf("Cannot create pipe...Client Exiting\n");
			exit(1);
		}
                if(pipe(pipe_2) < 0)
		{
			printf("Cannot create pipe...Client Exiting\n");
                        exit(1);
		}

                //Fork the server
                if((server_pid = fork())< 0)
                {
                        printf("Fork Failed..Client Exiting.\n");
                        exit(1);
                }
                if(server_pid == 0)
                {
                        if (close(pipe_1[1]) < 0)
			{
				printf("Cannot close Pipe1...Server Exiting...\n");
				exit(1);
			}
		
                        if(close(pipe_2[0]) < 0)
			{
				printf("Cannot close Pipe2...Server Exiting...\n");
                                exit(1);
			}

			signal(SIGINT, termination_handler);
                        server_pipe(pipe_1[0], pipe_2[1]);
                }
                else
                {   
                    int exit_client_flag =0;
                    
                    if(close(pipe_1[0]) < 0)
		    {
			printf("Cannot close the Pipe1 for reading.\n");
                        exit_client_flag = 1;
		    }
                    
                    if(exit_client_flag == 0)
                    {
                        if(close(pipe_2[1]) < 0)
                        {
                            printf("Cannot close the Pipe2 for writing.\n\n");
                            exit_client_flag = 1;
                        }
                    }

                    while(exit_client_flag == 0)
                    {        
                        Mesg msgToSend = getUserInputRequest();
                        
                        if(msgToSend.mesg_type == EXIT)
                        {
                            break;
                        }
                        
                        //Write to the server
                        if(write(pipe_1[1], &msgToSend, sizeof(Mesg)) < 0)
			{
				printf ("Error in sending request to the server in PIPES...\n");
                                break;
			}

                        //Read from the server
                        int n;
                        Mesg msgToRead;
                        if ((n = read(pipe_2[0], &msgToRead, sizeof(Mesg))) >= 0)
                        {
                            printf("Client read from the pipe: %s\n",  msgToRead.mesg_data);
                        }
                        else
                        {
                            printf ("Error in receiving response from the server in PIPES...\n");
                            break;
                        }
                    }
                    
                    //Perform cleanups
                    
                    //Kill the server
                    printf("Sending SIGINT to server\n");
                    if(kill(server_pid, SIGINT))
                    {
                        printf("Could not cleanly kill server process %d", server_pid);
                    }
                    
                    printf("Client Exiting...\n");
                }
                
                break;
            }
            case USE_FIFO:
            {
                int readfd, writefd;
                
                //Deleting existing(if) FIFOs
		if(access(FIFO1, F_OK) != -1)
		{
			printf("A rogue FIFO %s already exists..deleting the FIFO.\n", FIFO1);
			
                        if(unlink(FIFO1) == -1 )
			{
				printf("Error occurred while deleting existing FIFO %s...program exiting.\n", FIFO1);
				exit(1);
			}
		}
		
		if(access(FIFO2, F_OK) != -1)
                {
                        printf("A rogue FIFO %s already exists..deleting the FIFO.\n", FIFO2);
			
                        if(unlink(FIFO2) == -1 )
			{
				printf("Error occurred while deleting existing FIFO %s...program exiting.\n", FIFO2);
				exit(1);
			}
		}	
                
                //Create FIFO
                if(mkfifo(FIFO1, 0777) == -1)
		{
			printf("Cannot create the FIFO %s...Client Exiting\n", FIFO1);
			exit(1);
		}
	
                if(mkfifo(FIFO2, 0777) == -1)
		{
			printf("Cannot create the FIFO %s...Client Exiting\n", FIFO2);
                        
                        //Deleting FIFO1
                        printf("Deleting FIFO %s.", FIFO1);
                        
                        if(unlink(FIFO1) == -1 )
                        {
                            printf("Error occurred while deleting existing FIFO %s...program exiting.\n", FIFO1);
                        }
                        
                        exit(1);
		}
                
                //Fork the server process
                if((server_pid = fork())< 0)
                {
                        printf("Fork Failed!\n");
                        
                        //Deleting FIFOs
                        printf("Deleting the FIFOs..\n");
                        if(unlink(FIFO1) == -1 )
                        {
                                printf("Error occurred while deleting existing FIFO %s.\n", FIFO1);     
                        }
                        if(unlink(FIFO2) == -1 )
                        {
                                printf("Error occurred while deleting existing FIFO %s.\n", FIFO2);
                        }

                        printf("Client Exiting...\n");
                        
                        exit(1);
                }
                if(server_pid == 0)
                {
                    readfd = open(FIFO1, O_RDONLY,0);
		    if(readfd < 0)
		    {
			printf("Cannot open the FIFO1..Server Exiting...\n");
			exit(1);
		    }

                    writefd = open(FIFO2, O_WRONLY, 0); 
        	    if(writefd < 0)
                    {
                        printf("Cannot open the FIFO2..Server Exiting...\n");
                        exit(1);
                    }
            
		    signal(SIGINT, termination_handler);
	            
		    server_fifo(readfd, writefd);
                }
                else
                {
                    int exit_client_flag = 0 ;
                    
                    //open the FIFO1 for writing
                    writefd = open(FIFO1, O_WRONLY, 0);
	            if(writefd < 0)
                    {
                        printf("Cannot open the FIFO %s.\n", FIFO1);
                        exit_client_flag =1;
                    }
			
                    //open the FIFO2 for reading
                    if(exit_client_flag == 0)
                    {
                        readfd  = open(FIFO2, O_RDONLY, 0);
                        if(readfd < 0)
                        {
                            exit_client_flag =1;
                        }
                    }

                    while(exit_client_flag == 0)
                    {        
                        Mesg msgToSend = getUserInputRequest();
                        
                        if(msgToSend.mesg_type == EXIT)
                        {
			    break;
                        }
                        
                        //Write to the server
                        if(write(writefd, &msgToSend, sizeof(Mesg))< 0)
			{
				printf ("Error in sending request to the server in FIFO...\n");
                		break;
			}

                        //Read from the server
                        int n;
                        Mesg msgToRead;
			if ((n = read(readfd, &msgToRead, sizeof(Mesg))) >= 0)
                        {
                             printf("Client read from the fifo: %s\n",  msgToRead.mesg_data);
                        }
                        else
                        {
                            printf ("Error in receiving response from the server in FIFO...\n");
                            break;
                        }
                    }
                    
                    //Perform Cleanups
                    
                    //Kill the server
                    printf("Sending SIGINT to server\n");
                    if(kill(server_pid, SIGINT))
                    {
                        printf("Could not cleanly kill server process %d", server_pid);
                    }
                    
                    //Deleting FIFOs
                    printf("Deleting the FIFOs..\n");
                    if(unlink(FIFO1) == -1 )
                    {
                            printf("Error occurred while deleting existing FIFO %s.\n", FIFO1);     
                    }
                    if(unlink(FIFO2) == -1 )
                    {
                            printf("Error occurred while deleting existing FIFO %s.\n", FIFO2);
                    }
                   
                    printf("Client Exiting...\n");
                }
                
                break;
            }
            case USE_POMQ:
            {
                mqd_t readfd, writefd;
                struct mq_attr mqAttr;
                mqAttr.mq_maxmsg = 10;
                mqAttr.mq_msgsize = POMQ_MSGSIZE;

		//Check if the Posix Messge Queues already exist.. If they exist then delete them.
		if(mq_open(MQNAME, O_RDWR) >= 0)
                {
                        printf("A rogue Queue %s already exists..deleting the queue.\n", MQNAME);
                        
			if(mq_unlink(MQNAME) == -1 )
			{
				printf("Error occurred while deleting existing Posix Message Queue %s...program exiting.\n", MQNAME);
				exit(1);
			}
		}
		
		if(mq_open(MQNAME_2, O_RDWR) >= 0)
                {
                        printf("A rogue Queue %s already exists..deleting the queue.\n", MQNAME_2);
                        
                	if(mq_unlink(MQNAME_2) == -1)
			{
				printf("Error occurred while deleting existing Posix Message Queue %s...program exiting.\n", MQNAME_2);
				exit(1);
			}
		}
        
		//Create the new Posix Message Queues in the client
                readfd = mq_open (MQNAME, O_RDWR|O_CREAT, S_IWUSR|S_IRUSR, &mqAttr);
                if(readfd < 0)
                {
                    printf("Cannot create the Posix message queue %s..program exiting.", MQNAME);
                    exit(1);
                }
        
                writefd = mq_open (MQNAME_2, O_RDWR|O_CREAT, S_IWUSR|S_IRUSR, &mqAttr);
                if(writefd < 0)
		{
		    printf("Cannot create the Posix message queue %s..program exiting.", MQNAME_2);
                    
                    //Deleting the MQNAME
                    if(mq_unlink(MQNAME) == -1)
                    {
                        printf("Error occurred while deleting existing Posix Message Queue %s....\n", MQNAME);
                    }
                    
                    exit(1);
		}
        
		//Fork the server
                if((server_pid = fork())< 0)
                {
                    printf("Fork Failed!\n");
                    
                    //Deleting the message queues
                    printf("Deleting Message Queues.\n");
                    
                    if(mq_unlink(MQNAME) == -1 )
                    {
                        printf("Error occurred while deleting existing Posix Message Queue %s...\n", MQNAME);
                    }

                    if(mq_unlink(MQNAME_2) == -1)
                    {
                        printf("Error occurred while deleting existing Posix Message Queue %s....\n", MQNAME_2);
                    }

                    printf("Client Exiting...\n");
                    
                    exit(1);
                }
                
                if(server_pid == 0)
                {
		    //Open the Posix Message Queue in the server
                    writefd = mq_open(MQNAME, O_RDWR);
                    if(writefd < 0)
		    {
			printf("Cannot open the Posix message queue %s..program exiting.", MQNAME);
                    	exit(1);
		    }
                
                    readfd = mq_open(MQNAME_2, O_RDWR);
                    if(readfd < 0)
                    {
                        printf("Cannot open the Posix message queue %s..program exiting.", MQNAME_2);
                        exit(1);
                    }
		   
                    server_POMQ(readfd, writefd);
                }
                else
                {
                    while(1)
                    {        
                        Mesg msgToSend = getUserInputRequest();
                        
                        if(msgToSend.mesg_type == EXIT)
                        {
			    break;
                        }
                        
                        //Write to the server
                        if(mq_send (writefd, (char *)&msgToSend, sizeof(Mesg), 1) < 0)
                        {
                            printf ("Error occurred while sending request to the server in Posix Message Queues...\n");
                            break;
                        }
                        
                        //Read from the server
                        Mesg msgToRead;

                        if(mq_receive (readfd, (char *)&msgToRead, POMQ_BUFSIZE, NULL) < 0)
                        {
                            printf ("Error occurred while receiving response from the server in Posix Message Queues...\n");
                            break;
                        }
                        else
                        {
                             printf("Client read from the Posix Message Queue: %s\n",  msgToRead.mesg_data);
                        }
                    }
                    
                    //Perform Cleanups
                    
                    //Kill Server
                    printf("Sending SIGINT to server\n");
                    
                    if(kill(server_pid, SIGINT))
                    {
                        printf("Could not cleanly kill server process %d", server_pid);
                    }

                    //Deleting the message queues
                    printf("Deleting Message Queues.\n");
                    
                    if(mq_unlink(MQNAME) == -1 )
                    {
                        printf("Error occurred while deleting existing Posix Message Queue %s...\n", MQNAME);
                    }

                    if(mq_unlink(MQNAME_2) == -1)
                    {
                        printf("Error occurred while deleting existing Posix Message Queue %s....\n", MQNAME_2);
                    }

                    printf("Client Exiting...\n");
                }
                
                break;
            }
	    case USE_SVMQ:
	    {	
		int check_readfd, check_writefd;
		check_readfd = msgget(MQ_KEY1, 0);
                if(check_readfd > 0)
		{
			printf("A rogue Queue %s already exists..deleting the queue.\n", MQ_KEY1);
			if (msgctl(check_readfd, IPC_RMID, NULL) == -1)
                        {
                                printf("Cannot delete the System V message queue %s Properly.", MQ_KEY1);
				exit(1);
                        }
		}	
	
		check_writefd= msgget(MQ_KEY2, 0);
		if(check_writefd > 0)
                {
                        printf("A rogue Queue %s already exists..deleting the queue.\n", MQ_KEY2);
                        if (msgctl(check_writefd, IPC_RMID, NULL) == -1)
                        {
                                printf("Cannot delete the System V message queue %s Properly.", MQ_KEY2);
                                exit(1);
                        }
                }
			
		//Create the System V Message Queues
		int readfd, writefd;
		
                readfd = msgget(MQ_KEY1, 0777 | IPC_CREAT);
		if(readfd < 0)
		{
			printf("Cannot create the System V message queue %s..program exiting.", MQ_KEY1);
                        exit(1);
		}

        	writefd = msgget(MQ_KEY2, 0777 | IPC_CREAT);
		if(writefd < 0)
                {
                        printf("Cannot create the System V message queue %s..program exiting.", MQ_KEY2);
                        
                        //Deleting the MQ_KEY1
                        if (msgctl(readfd, IPC_RMID, NULL) == -1) 
                        {
                            printf("Cannot delete the System V message queue Properly.");
                        }
                        
                        exit(1);
                }
		
                if((server_pid = fork())< 0)
                {
                        printf("Fork Failed!\n");
                        
                        //Delete the System V Message queues
                        printf("Deleting System V Message Queues.\n");
                        
                        if (msgctl(readfd, IPC_RMID, NULL) == -1) 
                        {
                            printf("Cannot delete the System V message queue Properly.");
                        }

                        if (msgctl(writefd, IPC_RMID, NULL) == -1)
                        {
                            printf("Cannot delete the System V message queue Properly.");
                        }

                        printf("Client Exiting...\n");
                        
                        exit(1);
                }
                if(server_pid == 0)
                {
			writefd = msgget(MQ_KEY1, 0);
                	if(writefd < 0)
                	{
                        	printf("Cannot get the key to  the System V message queue %s..server exiting.", MQ_KEY1);
				exit(1);
                	}
		
			readfd = msgget(MQ_KEY2, 0);
			if(readfd < 0)
                        {
                                printf("Cannot get the key to  the System V message queue %s..server exiting.", MQ_KEY2);
				exit(1);
                        }

                        signal(SIGINT, termination_handler);
                	server_system_v_mq(readfd, writefd);
                }
                else
                {
                    while(1)
                    {
                        Mesg msgToSend = getUserInputRequest();

                        if(msgToSend.mesg_type == EXIT)
                        {
		            break;
                        }

                        //Write to the server
                        if(msgsnd(writefd, &msgToSend, sizeof(Mesg), 0) == -1)
                        {
                            printf("Error in sending client request to the server in the System V Message Queue.\n");
                            break;
                        }

                        //Read from the server
                        Mesg msgToRead;
			if ((msgrcv(readfd, &msgToRead, sizeof(Mesg), 0, 0)) !=-1)
                        {
                        	printf("Client read from the System V Message Queue: %s\n",  msgToRead.mesg_data);
                        }
                        else
                        {
                            printf("Error in receiving server response in the System V Message Queue.\n");
                            break;
                        }
                    }
                    
                    //Perform Cleanups
                    
                    //Kill Server
                    
                    printf("Sending SIGINT to server\n");

                    if(kill(server_pid, SIGINT))
                    {
                        printf("Could not cleanly kill server process %d", server_pid);
                    }
                    
                    //Delete the System V Message queues
                    printf("Deleting System V Message Queues.\n");
                    if (msgctl(readfd, IPC_RMID, NULL) == -1) 
                    {
                        printf("Cannot delete the System V message queue Properly.");
                    }

                    if (msgctl(writefd, IPC_RMID, NULL) == -1)
                    {
                        printf("Cannot delete the System V message queue Properly.");
                    }

                    printf("Client Exiting...\n");
                }

                break;
	    }
            default:
            {
                printf("invalid IPC selected. Terminating\n");
                break;
            }
	}
	
        return 0;
}

